---
bibliography: references.bib
---

# Ricerche svolte per confezionare compiti su Taper functions

## Sequenza

1.  Garcia [-@dyntaper], presentazione teoria 'dynamic tree form

2.  Koirala et al. [-@koirala2021], profili pre teak in Nepal, quello di Garcia è il modello selezionato

3.  Garcia [-@garcia_dynamic_2022], pubblicazione del pacchetto R 'dyntaper'

\-\-\--

-   Wijenayake et al. [-@wijenayake2019], teak in Sri Lanka, funzioni di profilo per la gestione forestale

## TESTI prodotti

["Funzioni di profilo per la gestione forestale"](https://rpubs.com/scotti/taper_functions_in_forest_management)

['dyntaper' introduction](https://rpubs.com/scotti/dyntaper_intro)

## References
